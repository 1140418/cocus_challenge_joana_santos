# Setup

### Build and Deploy API

For use this api, it is necessary that you have postgres and docker installed in the machine.
So you should execute the following commands:

```bash
git clone https://1140418@bitbucket.org/1140418/cocus_challenge_joana_santos.git
cd REPOSITORY_PACKAGE/api
mvn clean package
docker build -t NAME_OF_IMAGE .
cd ..
docker-compose up
```

### Use
For use this api you can do the following request:
```bash
http://localhost:9090/flight/avg?origin=NAME_OF_ORIGIN&dest=NAME_OF_DESTINATION&dest=...&dateFrom=FROM_DATE&dateTo=TO_DATE&curr=CURRENCY
```

Example:
```bash
http://localhost:9090/flight/avg?origin=LIS&dest=OPO&dateFrom=2019/11/06&dateTo=2019/11/08&curr=EUR
```

# Logic

In this api, you can calculate the average prices of flights and bags for a list of flight from origin to some destinations in specific dates choose by you.

The result of this requests are showed to you and the information that you use in the request is saved in database for future information. You can request all the information or delete all the records. 
For that, I used a distribution in layers (controller, service, repository, model and dto).  This allows a better organization of code, which simplify the way of change functionalities in the future.

