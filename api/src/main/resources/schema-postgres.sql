CREATE TABLE public.request (
	id SERIAL PRIMARY KEY,
	created_date date NOT NULL,
	curr varchar(255) NULL,
	date_from timestamp NULL,
	date_to timestamp NULL
);


CREATE TABLE public.request_dest (
	request_id int8 NOT NULL REFERENCES public.request(id),
	dest varchar(255) NULL
);