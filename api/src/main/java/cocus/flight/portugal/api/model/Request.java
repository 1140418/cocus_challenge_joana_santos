package cocus.flight.portugal.api.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "request")
@NoArgsConstructor
public class Request {

    /**
     * Id of request.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Getter
    private Long requestId;

    /**
     * Date of execution the request
     */
    @CreatedDate
    @Column(name = "created_date")
    private Date createdDate;

    /**
     * List of destinations.
     */
    @ElementCollection
    @Getter
    @Setter
    private List<String> dest;

    /**
     * Date from.
     */
    @Column
    @Getter
    @Setter
    private Date dateFrom;

    /**
     * Date to.
     */
    @Column
    @Getter
    @Setter
    private Date dateTo;

    /**
     * Currency.
     */
    @Column
    @Getter
    @Setter
    private String curr;

    /**
     * Initialize new request.
     * 
     * @param dest        list of destinations
     * @param createdDate date of execution the request
     * @param dateFrom    date from
     * @param dateTo      date to
     * @param curr        currency
     */
    public Request(List<String> dest, Date createdDate, Date dateFrom, Date dateTo, String curr) {
        this.dest = dest;
        this.createdDate = createdDate;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.curr = curr;
    }

}