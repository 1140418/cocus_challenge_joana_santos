package cocus.flight.portugal.api.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cocus.flight.portugal.api.dto.FlightAverageDTO;
import cocus.flight.portugal.api.model.JsonHolder;
import cocus.flight.portugal.api.model.Request;
import cocus.flight.portugal.api.repository.RequestRepository;
import cocus.flight.portugal.api.service.AverageService;
import cocus.flight.portugal.api.service.FlightService;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class FlightController {

    @Autowired
    private AverageService averageService;

    @Autowired
    private FlightService flightService;

    @Autowired
    private RequestRepository requestRepository;

    /**
     * Endpoint to request the average of price and bags.
     * 
     * @param origin       origin of the flight
     * @param destinations list of destionation of flights
     * @param dateFrom     date from
     * @param dateTo       date to
     * @param curr         currency to show the prices
     * @return flight average dto
     */
    @RequestMapping(value = "/flight/avg", produces = "application/json", method = RequestMethod.GET)
    public ResponseEntity<?> getFlightPriceAverage(@RequestParam("origin") String origin,
            @RequestParam("dest") List<String> destinations, @RequestParam("dateFrom") String dateFrom,
            @RequestParam("dateTo") String dateTo, @RequestParam("curr") String curr) {
        if (!destinations.isEmpty() && origin != null && origin != "" && dateFrom != "" && dateFrom != null
                && dateTo != "" && dateTo != null && curr != "" && curr != null) {
            log.info("Request flight average to " + destinations + " from " + dateFrom + " to " + dateTo + " in "
                    + curr);
            try {
                SimpleDateFormat dt1 = new SimpleDateFormat("yyyy/MM/dd");
                Request r = new Request(destinations, Calendar.getInstance().getTime(), dt1.parse(dateFrom),
                        dt1.parse(dateTo), curr);
                Map<String, FlightAverageDTO> dtos = new HashMap<String, FlightAverageDTO>();
                requestRepository.save(r);
                for (String d : destinations) {
                    JsonHolder response = flightService.callKiwiAPI(origin, d, dateFrom, dateTo, curr);
                    if (response != null) {
                        double priceAvg = averageService.calculateFlightsPriceAverage(response.getData());
                        Map<String, Double> bags = averageService.calculateFlightsBagsAverage(response.getData());

                        FlightAverageDTO dto = new FlightAverageDTO(curr, priceAvg, bags);
                        dtos.put(d, dto);
                    }
                }
                if (!dtos.isEmpty()) {
                    return ResponseEntity.ok().body(dtos);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return ResponseEntity.badRequest().build();
    }
}