package cocus.flight.portugal.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cocus.flight.portugal.api.model.Request;

public interface RequestRepository extends JpaRepository<Request, Long> {

}