package cocus.flight.portugal.api.dto;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FlightAverageDTO {

    /**
     * Currency of average calculation
     */
    @Getter
    @Setter
    private String currency;

    /**
     * Average of price
     */
    @Getter
    @Setter
    private double price_average;

    /**
     * List of average bags price.
     */
    @Getter
    @Setter
    private Map<String, Double> bags_price;

    /**
     * Initialize new flightAverageDTO
     * 
     * @param currency      currency to show
     * @param price_average average of flights price
     * @param bags_price    average of flights bags prices
     */
    public FlightAverageDTO(String currency, double price_average, Map<String, Double> bags_price) {
        this.currency = currency;
        this.price_average = price_average;
        this.bags_price = bags_price;

    }
}