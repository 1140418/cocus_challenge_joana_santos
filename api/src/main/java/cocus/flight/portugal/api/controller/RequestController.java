package cocus.flight.portugal.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cocus.flight.portugal.api.repository.RequestRepository;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class RequestController {

    @Autowired
    private RequestRepository requestRepository;

    /**
     * Endpoint to show all the records of requests.
     * 
     * @return list of records of requests
     */
    @RequestMapping(value = "/request", produces = "application/json", method = RequestMethod.GET)
    public ResponseEntity<?> getAllRecordsOfRequests() {
        log.info("Request all records of requests");
        return ResponseEntity.ok().body(requestRepository.findAll());
    }

    /**
     * Endpoint to delete all the records of requets.
     * 
     * @return message of success
     */
    @RequestMapping(value = "/request", produces = "application/json", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteAllRecordsOfRequests() {
        log.info("Delete all records of requests");
        requestRepository.deleteAll();
        return ResponseEntity.ok().build();
    }

}