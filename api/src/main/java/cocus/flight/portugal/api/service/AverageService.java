package cocus.flight.portugal.api.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import cocus.flight.portugal.api.model.Flight;

@Service
public class AverageService {

    /**
     * Allows to calculate the average price of fligths.
     * 
     * @param flights list of flights
     * @return value of average price of flights
     */
    public double calculateFlightsPriceAverage(List<Flight> flights) {
        int flightsCount = flights.size();
        double flightPriceSum = 0;
        for (Flight f : flights) {
            flightPriceSum += f.getPrice();
        }
        return flightPriceSum / flightsCount;
    }

    /**
     * Allows to calculate the average price of bags.
     * 
     * @param flights list of flights
     * @return list of values of average price of bags
     */
    public Map<String, Double> calculateFlightsBagsAverage(List<Flight> flights) {
        Map<String, Double> bags = new HashMap<String, Double>();
        int flightsCount = flights.size();
        double flightBag1Sum = 0;
        double flightBag2Sum = 0;
        for (Flight f : flights) {
            if (f.getBags_price().get("1") != null) {
                flightBag1Sum += f.getBags_price().get("1");
            }
            if (f.getBags_price().get("2") != null) {
                flightBag2Sum += f.getBags_price().get("2");
            }
        }
        bags.put("1", flightBag1Sum / flightsCount);
        bags.put("2", flightBag2Sum / flightsCount);
        return bags;
    }

}