package cocus.flight.portugal.api.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import cocus.flight.portugal.api.model.JsonHolder;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class FlightService {

    private static final String API_URL = "https://api.skypicker.com/flights";

    /**
     * Allows to call Kiwi API.
     * 
     * @param origin   origin of flight
     * @param d        destination of flight
     * @param dateFrom date from
     * @param dateTo   date to
     * @param curr     currency
     * @return info of the request
     */
    public JsonHolder callKiwiAPI(String origin, String d, String dateFrom, String dateTo, String curr) {
        String url = "";
        try {
            SimpleDateFormat dt1 = new SimpleDateFormat("yyyy/MM/dd");
            SimpleDateFormat dt2 = new SimpleDateFormat("dd/MM/yyyy");
            url = API_URL + "?flyFrom=" + origin + "&to=" + d + "&date_from=" + dt2.format(dt1.parse(dateFrom))
                    + "&date_to=" + dt2.format(dt1.parse(dateTo)) + "&partner=picky&select_airlines=FR,TP&curr=" + curr;
            log.info(url);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        RestTemplate restTemplate = new RestTemplate();
        if (!url.equals("")) {
            return restTemplate.getForObject(url, JsonHolder.class);
        } else {
            return null;
        }

    }

}