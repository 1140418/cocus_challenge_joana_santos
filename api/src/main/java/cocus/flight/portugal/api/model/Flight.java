package cocus.flight.portugal.api.model;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Flight {

    /**
     * Origin of flight.
     */
    private String cityCodeFrom;

    /**
     * Destination of flight.
     */
    private String cityCodeTo;

    /**
     * Price of the flight
     */
    private double price;

    /**
     * List of bags prices.
     */
    private Map<String, Double> bags_price;

    /**
     * Initialize new flight.
     * 
     * @param cityCodeFrom origin of flight
     * @param cityCodeTo   destination of flight
     * @param price        price of flight
     * @param bags_price   list of bags prices
     */
    public Flight(String cityCodeFrom, String cityCodeTo, double price, Map<String, Double> bags_price) {
        this.cityCodeFrom = cityCodeFrom;
        this.cityCodeTo = cityCodeTo;
        this.price = price;
        this.bags_price = bags_price;
    }

}