package cocus.flight.portugal.api.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import cocus.flight.portugal.api.ApiApplication;
import cocus.flight.portugal.api.model.Flight;
import cocus.flight.portugal.api.model.JsonHolder;
import cocus.flight.portugal.api.service.FlightService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = ApiApplication.class)
@ActiveProfiles("test")
public class FlightControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @MockBean
    private FlightService flightService;

    @Before
    public void setUp() throws Exception {
        JsonHolder js = new JsonHolder();
        js.setSearch_id("e1f8edc5-b701-4679-9957-ee94b58e25aa");
        List<Flight> flights = new ArrayList<Flight>();
        Map<String, Double> bags_price = new HashMap<String, Double>();
        bags_price.put("1", 26.0);
        bags_price.put("2", 84.5);
        Flight f1 = new Flight("OPO", "LIS", 86, bags_price);
        flights.add(f1);
        js.setData(flights);
        when(flightService.callKiwiAPI(any(), any(), any(), any(), any())).thenReturn(js);
    }

    @Test
    public void getFlightPriceAverageTest() throws Exception {
        HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(headers);

        ResponseEntity<String> response = restTemplate.exchange(
                "/flight/avg?origin=LIS&dest=OPO&dateFrom=2019/11/01&dateTo=2019/11/02&curr=EUR", HttpMethod.GET,
                entity, String.class);
        String expected = "{\"OPO\":{\"currency\":\"EUR\",\"price_average\":86.0,\"bags_price\":{\"1\":26.0,\"2\":84.5}}}";
        
        assertThat(HttpStatus.OK).isEqualTo(response.getStatusCode());
        assertThat(expected).isEqualTo(response.getBody());

    }
}