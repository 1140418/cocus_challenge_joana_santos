package cocus.flight.portugal.api.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cocus.flight.portugal.api.model.Flight;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase
public class AverageServiceTest {

    @Autowired
    private AverageService averageService;

    @Test
    public void calculateFlightsPriceAverageTeste() {
        List<Flight> flights = new ArrayList<Flight>();
        Map<String, Double> bags_price = new HashMap<String, Double>();
        bags_price.put("1", 26.0);
        bags_price.put("2", 84.5);
        Flight f1 = new Flight("OPO", "LIS", 86.0, bags_price);
        Flight f2 = new Flight("OPO", "LIS", 88.0, bags_price);
        flights.add(f1);
        flights.add(f2);

        assertThat(87.0).isEqualTo(averageService.calculateFlightsPriceAverage(flights));
    }

    @Test
    public void calculateFlightsBagsAverageTest() {
        List<Flight> flights = new ArrayList<Flight>();
        Map<String, Double> bags_price = new HashMap<String, Double>();
        bags_price.put("1", 26.0);
        bags_price.put("2", 84.5);
        Flight f1 = new Flight("OPO", "LIS", 86.0, bags_price);
        Map<String, Double> bags_price1 = new HashMap<String, Double>();
        bags_price1.put("1", 28.0);
        bags_price1.put("2", 86.5);
        Flight f2 = new Flight("OPO", "LIS", 88.0, bags_price1);
        flights.add(f1);
        flights.add(f2);

        assertThat(27.0).isEqualTo(averageService.calculateFlightsBagsAverage(flights).get("1"));
        assertThat(85.5).isEqualTo(averageService.calculateFlightsBagsAverage(flights).get("2"));
    }

}