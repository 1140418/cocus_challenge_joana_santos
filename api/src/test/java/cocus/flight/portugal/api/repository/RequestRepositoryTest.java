package cocus.flight.portugal.api.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import cocus.flight.portugal.api.model.Request;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase
public class RequestRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private RequestRepository requestRepository;

    @Before
    public void setUp() throws Exception {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2019);
        cal.set(Calendar.MONTH, Calendar.NOVEMBER);
        cal.set(Calendar.DAY_OF_MONTH, 6);

        Calendar cal1 = Calendar.getInstance();
        cal1.set(Calendar.YEAR, 2019);
        cal1.set(Calendar.MONTH, Calendar.NOVEMBER);
        cal1.set(Calendar.DAY_OF_MONTH, 7);

        List<String> dest = new ArrayList<String>();
        dest.add("OPO");
        Request r = new Request(dest, Calendar.getInstance().getTime(), cal.getTime(), cal1.getTime(), "EUR");
        entityManager.persist(r);
        entityManager.flush();
    }

    @Test
    public void findAllTest() {
        int size = requestRepository.findAll().size();
        assertThat(size).isEqualTo(1);
    }

}